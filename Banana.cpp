#include "Banana.h"


#include "Banana.h"
#include "SOIL.h"

Banana::Banana(Component *parent) : Point(parent) {
    this->score = 7;
}


void Banana::load(int time) {

    texture_id = SOIL_load_OGL_texture("./assets/banana.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                       SOIL_FLAG_INVERT_Y);
}

void Banana::update(int time) {

}

void Banana::render(int time) {
    auto tx_w = 40.0;
    auto tx_h = 40.0;

    glBindTexture(GL_TEXTURE_2D, texture_id);
    glPushMatrix();
    glTranslatef(this->x, this->y, 0);

    glBegin(GL_POLYGON);
    glTexCoord2f(0, 0);
    glVertex2f(0, 0);
    glTexCoord2f(1.0, 0);
    glVertex2f(tx_w, 0);
    glTexCoord2f(1.0, 1.0);
    glVertex2f(tx_w, tx_h);
    glTexCoord2f(0, 1.0);
    glVertex2f(0, tx_h);
    glEnd();

    glPopMatrix();
}
