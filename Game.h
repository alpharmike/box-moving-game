//
// Created by Sandman on 12/17/2020.
//

#ifndef PACMANPROJECT_GAME_H
#define PACMANPROJECT_GAME_H

#include "Component.h"
#include "Block.h"
#include <vector>
#include "Pacman.h"
#include "Ghost.h"
#include "Point.h"
#include "Cherry.h"
#include "Orange.h"
#include "Banana.h"
#include "Cherry.h"

// this library is used for playing sounds
//#include <irrKlang/irrKlang.h>


using namespace std;
//using namespace irrklang;

class Game : public Component {
public:
    enum GameStatus
    {
        Playing = 0, Won = 1, Loss = 2
    };
    Game();

    ~Game();

    void load(int currTime);

    void update(int time);

    void render(int time);

    void keyboard(int time, int key, int x, int y);

    void keyboard_up(int time, int key, int x, int y);

    bool isEmpty(float x, float y);

    bool checkCollision(float x0, float y0, float x1, float y1);

    void updatePoint(Point* point);

    void drawText(float r, float g, float b, int x, int y, string text, void *font);

    bool pacCollision(float x0, float y0, float x1, float y1);

    void resetGame();

    void toggleFullScreen();

    bool isFullScreen();

private:
    Pacman *pacman;
    std::vector<Ghost *> ghosts;
    std::vector<Component *> components;
    int totalPoint;
//    ISoundEngine *soundEngine;
    GameStatus gameStatus;
    bool fullScreen;

};

#endif //PACMANPROJECT_GAME_H
