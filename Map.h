//
// Created by Sandman on 12/18/2020.
//

#ifndef PACMANPROJECT_MAP_H
#define PACMANPROJECT_MAP_H

/**
 * B for Block
 * P for Point
 * G for Ghost
 * E for Empty
 */

enum MapSymbol {B, P, G, E};

MapSymbol map[25][25] = {
        B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B,
        B, E, E, E, B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B, E, E, E, B,
        B, E, E, E, B, E, E, B, B, B, B, E, E, E, B, B, B, B, E, E, B, E, E, E, B,
        B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, B, B, B, B, B, B, B, B, B, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B,
        B, E, B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B, E, B,
        B, E, B, E, E, E, E, E, E, B, E, E, E, E, E, B, E, E, E, E, E, E, B, E, B,
        B, E, B, B, B, E, E, E, E, B, E, E, E, E, E, B, E, E, E, E, B, B, B, E, B,
        B, E, B, E, E, E, E, E, E, B, B, B, B, B, B, B, E, E, E, E, E, E, B, E, B,
        B, E, B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B, E, B,
        B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, B, B, B, B, B, B, B, B, B, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B,
        B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E, B,
        B, E, E, E, B, E, E, B, B, B, B, E, E, E, B, B, B, B, E, E, B, E, E, E, B,
        B, E, E, E, B, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, B, E, E, E, B,
        B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B,
};

#endif //PACMANPROJECT_MAP_H
