#ifndef PACMANPROJECT_POINT_H
#define PACMANPROJECT_POINT_H

#include <iostream>
#include "GL/glut.h"
#include "Component.h"

using namespace std;

class Point : public Component {
public:
    Point(Component *parent);

    float getX();

    float getY();

    virtual void load(int time) = 0;

    virtual void update(int time) = 0;

    virtual void render(int time) = 0;

    void setPosition(float x, float y);

    int getScore();

protected:
    int score;
    float x, y;
    GLuint texture_id;
};


#endif //PACMANPROJECT_POINT_H
