#ifndef PACMANPROJECT_BLOCK_H
#define PACMANPROJECT_BLOCK_H

#include <iostream>
#include "GL/glut.h"
#include "Component.h"

using namespace std;

class Block : public Component {
public:
    Block(Component *parent);

    float getX();

    float getY();

    void load(int time);

    void update(int time);

    void render(int time);

    void setPosition(float x, float y, float angle);

private:
    float x, y, angle;
    GLuint texture_id;
};


#endif //PACMANPROJECT_BLOCK_H
