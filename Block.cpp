#include "Block.h"
#include "Texture.h"
#include "SOIL.h"

Block::Block(Component *parent) : Component(parent) {

}

float Block::getX() {
    return x;
}

float Block::getY() {
    return y;
}

void Block::load(int time) {
    Component::load(time);

    texture_id = SOIL_load_OGL_texture("./assets/brick.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                                    SOIL_FLAG_INVERT_Y);
}

void Block::update(int time) {

}

void Block::render(int time) {
//    drawTexture(texture_id, 70, 70, x, y, angle);
    auto tx_w = 40;
    auto tx_h = 40;

    glBindTexture(GL_TEXTURE_2D, texture_id);
    glPushMatrix();
    glTranslatef(this->x, this->y, 0);

    glBegin(GL_POLYGON);
    glTexCoord2f(0, 0);
    glVertex2f(0, 0);
    glTexCoord2f(1.0, 0);
    glVertex2f(tx_w, 0);
    glTexCoord2f(1.0, 1.0);
    glVertex2f(tx_w, tx_h);
    glTexCoord2f(0, 1.0);
    glVertex2f(0, tx_h);
    glEnd();

    glPopMatrix();
}

void Block::setPosition(float x, float y, float angle) {
    this->x = x;
    this->y = y;
    this->angle = angle;
}