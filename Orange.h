//
// Created by Sandman on 12/18/2020.
//

#ifndef PACMANPROJECT_ORANGE_H
#define PACMANPROJECT_ORANGE_H

#include "Point.h"

class Orange : public Point {
public:
    Orange(Component* parent);
    void load(int time);
    void update(int time);
    void render(int time);
};

#endif //PACMANPROJECT_ORANGE_H
