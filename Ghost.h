//
// Created by Sandman on 12/17/2020.
//

#ifndef PACMANPROJECT_GHOST_H
#define PACMANPROJECT_GHOST_H

#include "Component.h"

using namespace std;

class Ghost : public Component
{
public:

    enum GhostDirection
    {
        None = 0, Up = 1, Right = 2, Down = 3, Left = 4
    };

    Ghost(Component* parent);
    void load(int time);
    void update(int time);
    void render(int time);
    void setPosition(float x, float y);

    void up(int time);
    void down(int time);
    void left(int time);
    void right(int time);
    void stop(int time);

    float getX();
    float getY();

    float getNextX();
    float getNextY();

    void changeDirection(int time);

private:
    float x; float y;
    GLuint texture_id;
    int frame;

    int start_move_time;
    GhostDirection direction;
    bool is_moving;
};

#endif //PACMANPROJECT_GHOST_H
