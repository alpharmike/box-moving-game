//
// Created by Sandman on 12/17/2020.
//

#ifndef PACMANPROJECT_PACMAN_H
#define PACMANPROJECT_PACMAN_H

#include "Component.h"

#include <iostream>

class Pacman : public Component
{
public:

    enum CharacterDirection
    {
        None = 0, Up = 1, Right = 2, Down = 3, Left = 4
    };

    Pacman(Component* parent);
    void load(int time);
    void update(int time);
    void render(int time);
    void setPosition(int x, int y);

    void up(int time);
    void down(int time);
    void left(int time);
    void right(int time);
    void stop(int time);
    float getX();
    float getY();
    float getNextX();
    float getNextY();
    bool isHorizontal();
    bool isVertical();

private:
    float x; float y;
    GLuint texture_id;
    int frame;

    int start_move_time;
    CharacterDirection direction;
    bool is_moving;
protected:
    friend class Game;
};
#endif //PACMANPROJECT_PACMAN_H
