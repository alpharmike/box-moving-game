//
// Created by Sandman on 12/18/2020.
//

#ifndef PACMANPROJECT_CHERRY_H
#define PACMANPROJECT_CHERRY_H

#include "Point.h"

class Cherry : public Point {
public:
    Cherry(Component* parent);
    void load(int time);
    void update(int time);
    void render(int time);
};

#endif //PACMANPROJECT_CHERRY_H
