//
// Created by Sandman on 12/18/2020.
//

#ifndef PACMANPROJECT_BANANA_H
#define PACMANPROJECT_BANANA_H

#include "Component.h"
#include "Point.h"

class Banana : public Point {
public:
    Banana(Component* parent);
    void load(int time);
    void update(int time);
    void render(int time);
};

#endif //PACMANPROJECT_BANANA_H
