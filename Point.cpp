#include "Point.h"

Point::Point(Component *parent) : Component(parent) {
}

float Point::getX() {
    return x;
}

float Point::getY() {
    return y;
}


void Point::setPosition(float x, float y) {
    this->x = x;
    this->y = y;
}

int Point::getScore() {
    return this->score;
}