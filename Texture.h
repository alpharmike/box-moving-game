//
// Created by Sandman on 12/16/2020.
//

#ifndef PACMANPROJECT_TEXTURE_H
#define PACMANPROJECT_TEXTURE_H

#include <GL/glut.h>


void drawTexture(unsigned int texture, int length, int height, float x, float y, float angle);

GLuint loadTexture(const char *filename);

#endif //PACMANPROJECT_TEXTURE_H
