#include "Game.h"
#include "Map.h"
#include<ctime>

Game::Game() : Component(NULL) {
    this->game = this;
}

Game::~Game() {
}

bool Game::checkCollision(float x0, float y0, float x1, float y1) {
    return abs(x0 - x1) < 40 && abs(y0 - y1) < 40;
}

void Game::load(int currTime) {
//    this->soundEngine = createIrrKlangDevice();
    this->totalPoint = 0;
    gameStatus = Playing;
    fullScreen = false;

    // generating seed for random numbers
    srand(time(0));

    // random position for pacman
    int x = ((double) rand() / (RAND_MAX)) * 25;
    int y = ((double) rand() / (RAND_MAX)) * 25;
    while (map[x][y] != MapSymbol::E) {
        x = ((double) rand() / (RAND_MAX)) * 25;
        y = ((double) rand() / (RAND_MAX)) * 25;
    }

    this->pacman = new Pacman(this);
    this->pacman->setPosition(y * 40, (24 - x) * 40);
    components.push_back(this->pacman);

    // specifying block positions based on the map 2D array
    for (int i = 0; i < 25; ++i) {
        for (int j = 0; j < 25; ++j) {
            if (map[i][j] == MapSymbol::B) {
                auto *block = new Block(this);
                block->setPosition(40 * j, 40 * (24 - i), 0);
                components.push_back(block);
            }

        }
    }

    // generating random-position cherry points on empty positions
    for (int i = 0; i < 10; i++) {
        Point *point = new Cherry(this);
        int x = ((double) rand() / (RAND_MAX)) * 25;
        int y = ((double) rand() / (RAND_MAX)) * 25;
        if (map[x][y] == MapSymbol::E) {
            map[x][y] = MapSymbol::P;
            point->setPosition(y * 40, (24 - x) * 40);
            components.push_back(point);
        } else {
            --i;
        }
    }

    // generating random-position banana points on empty positions
    for (int i = 0; i < 5; i++) {
        Point *point = new Banana(this);
        int x = ((double) rand() / (RAND_MAX)) * 25;
        int y = ((double) rand() / (RAND_MAX)) * 25;
        if (map[x][y] == MapSymbol::E) {
            map[x][y] = MapSymbol::P;
            point->setPosition(y * 40, (24 - x) * 40);
            components.push_back(point);
        } else {
            --i;
        }
    }

    // generating random-position orange points on empty positions
    for (int i = 0; i < 5; i++) {
        Point *point = new Orange(this);
        int x = ((double) rand() / (RAND_MAX)) * 25;
        int y = ((double) rand() / (RAND_MAX)) * 25;
        if (map[x][y] == MapSymbol::E) {
            map[x][y] = MapSymbol::P;
            point->setPosition(y * 40, (24 - x) * 40);
            components.push_back(point);
        } else {
            --i;
        }
    }

    // generating random-position ghosts on empty positions
    for (int i = 0; i < 5; i++) {
        auto *ghost = new Ghost(this);
        int x = ((double) rand() / (RAND_MAX)) * 25;
        int y = ((double) rand() / (RAND_MAX)) * 25;
        if (map[x][y] == MapSymbol::E) {
            map[x][y] = MapSymbol::G;
            ghost->setPosition(y * 40, (24 - x) * 40);
            components.push_back(ghost);
            ghosts.push_back(ghost);
        } else {
            --i;
        }
    }

    for (auto iter = components.begin(); iter != components.end(); iter++)
        (*iter)->load(currTime);
}


void Game::update(int time) {
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    Point *point;
    Block *block;
    Ghost *ghost;

    bool won = true;

    if (gameStatus == GameStatus::Playing) {
        for (auto iter = components.begin(); iter != components.end(); iter++) {

            // check if we hit a point object, if so, increase total point
            if ((point = dynamic_cast<Point *>(*iter)) != nullptr) {
                won = false;
                if (checkCollision(pacman->getNextX(), pacman->getNextY(), point->getX(), point->getY())) {
                    updatePoint(point);
                    components.erase(iter--);
//                    soundEngine->play2D(R"(.\audio\powerup.wav)", false);
                }
            } else if ((block = dynamic_cast<Block *>(*iter)) != nullptr) {
                // check if pacman hits a block, if so, it should stop moving
                if (checkCollision(pacman->getNextX(), pacman->getNextY(), block->getX(), block->getY())) {
                    pacman->stop(time);
                }

                for (auto ghostIter = ghosts.begin(); ghostIter != ghosts.end(); ghostIter++) {
                    ghost = ghosts.at(ghostIter - ghosts.begin());
                    // check for the collision of ghosts with blocks, if so, they should change their moving direction
                    if (checkCollision(ghost->getNextX(), ghost->getNextY(), block->getX(), block->getY())) {
                        ghost->changeDirection(time);
                        ghost->stop(time);

                    }


                    // check for the collision of pacman with a ghost, if so, the game is over
                    if (checkCollision(pacman->getX(), pacman->getY(), ghost->getNextX(), ghost->getNextY())) {
//                        soundEngine->stopAllSounds();
//                        soundEngine->removeAllSoundSources();
                        gameStatus = Loss;
                    }
                }
            } else if ((ghost = dynamic_cast<Ghost *>(*iter)) != nullptr) {
                // check for the collision of a ghost with another ghost, if so, they should change their moving direction
                for (Ghost *currGhost : ghosts) {
                    if (currGhost != ghost &&
                        checkCollision(ghost->getNextX(), ghost->getNextY(), currGhost->getX(), currGhost->getY())) {
                        ghost->changeDirection(time);
                        ghost->stop(time);
                    }
                }
            }
        }

        for (auto iter = components.begin(); iter != components.end(); iter++)
            (*iter)->update(time);

        // change the game status if all points are removed from the components vector
        if (won) {
            gameStatus = Won;
        }
    }


}

void Game::render(int time) {
    drawText(1, 0, 0, 1050, 900, "TOTAL POINT", GLUT_BITMAP_HELVETICA_18);
    drawText(0, 1, 0, 1140, 800, to_string(game->totalPoint), GLUT_BITMAP_TIMES_ROMAN_24);
    drawText(0, 0, 1, 1050, 700, "Q - Quit", GLUT_BITMAP_TIMES_ROMAN_24);
    drawText(0, 0, 1, 1050, 600, "R - Restart", GLUT_BITMAP_TIMES_ROMAN_24);
    drawText(0, 0, 1, 1050, 500, "F11 - Screen", GLUT_BITMAP_TIMES_ROMAN_24);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    for (auto &component : components)
        component->render(time);

    glDisable(GL_TEXTURE_2D);
    if (gameStatus == GameStatus::Loss) {
        drawText(1, 0, 0, 500, 500, "GAME OVER", GLUT_BITMAP_TIMES_ROMAN_24);
    } else if (gameStatus == GameStatus::Won) {
        drawText(0, 1, 0, 500, 500, "YOU WON", GLUT_BITMAP_TIMES_ROMAN_24);
    }
}

void Game::keyboard(int time, int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_UP:
            this->pacman->up(time);
            break;
        case GLUT_KEY_DOWN:
            this->pacman->down(time);
            break;
        case GLUT_KEY_LEFT:
            this->pacman->left(time);
            break;
        case GLUT_KEY_RIGHT:
            this->pacman->right(time);
            break;
    }
}

void Game::keyboard_up(int time, int key, int x, int y) {
    this->pacman->stop(time);
}

void Game::updatePoint(Point *point) {
    this->totalPoint += point->getScore();
}

void Game::drawText(float r, float g, float b, int x, int y, string text, void *font) {
    glColor4f(r, g, b, 1);
    glRasterPos2f(x, y); //define position on the screen
    for (char i : text) {
        glutBitmapCharacter(font, i);
    }
}

void Game::resetGame() {
//    this->soundEngine->stopAllSounds();
//    this->soundEngine->removeAllSoundSources();

    this->ghosts.clear();
    this->components.clear();
    this->totalPoint = 0;
}

void Game::toggleFullScreen() {
    this->fullScreen = !this->fullScreen;
}

bool Game::isFullScreen() {
    return this->fullScreen;
}