#include "Texture.h"
#include "include/SOIL/SOIL.h"
#include <iostream>

void drawTexture(unsigned int texture, int length, int height, float x, float y, float angle) {
    // Begin new transformation matrix

    glPushMatrix();

    int halfLength = length;
    int halfHeight = height;

    // Translate to center of sprite and rotate if necessary
    glTranslatef((float) x, (float) y, 0.0f);
    glRotatef(angle, 0.0f, 0.0f, 1.0f);

    // Enable texturing and bind selected sprite
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

    // Draw sprite shape as square to be textured of size (length,height)
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f);      // Bottom left
    glVertex2i(0, 0);
    glTexCoord2f(1.0f, 0.0f);      // Bottom right
    glVertex2i(halfLength, 0);
    glTexCoord2f(1.0f, 1.0f);      // Top right
    glVertex2i(halfLength, halfHeight);
    glTexCoord2f(0.0f, 1.0f);      // Top left
    glVertex2i(0, halfHeight);
    glEnd();

    glDisable(GL_TEXTURE_2D);

    // Pop matrix to ignore above transformations on future objects
    glPopMatrix();
}

GLuint loadTexture(const char *filename) {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    int tx_width, tx_height;
    unsigned char *tex;
    tex = SOIL_load_image(filename, &tx_width, &tx_height, 0, SOIL_LOAD_RGBA);

    GLuint tex_handle = 0;
    glGenTextures(1, &tex_handle);

    // create a new texture object and bind it to tex_handle
    glBindTexture(GL_TEXTURE_2D, tex_handle);

    // so we can blend it
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0,
                 GL_RGBA, tx_width, tx_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex);

    glBindTexture(GL_TEXTURE_2D, 0);
    return tex_handle;
}