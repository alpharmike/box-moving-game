//
// Created by Sandman on 12/17/2020.
//

#include "Ghost.h"
#include "SOIL.h"

Ghost::Ghost(Component *parent) : Component(parent) {

}

void Ghost::load(int time) {
    Component::load(time);

    direction = GhostDirection::Right;

    texture_id = SOIL_load_OGL_texture("./assets/ghost_sprite.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
                                       SOIL_FLAG_INVERT_Y);
}

void Ghost::update(int time) {
    this->frame = (time - this->start_move_time) / 100;
    auto diff_time = (time - this->start_move_time);

    switch (this->direction) {
        case GhostDirection::Up:
            this->y += .15;
            break;
        case GhostDirection::Down:
            this->y -= .15;
            break;
        case GhostDirection::Left:
            this->x -= .15;
            break;
        case GhostDirection::Right:
        default:
            this->x += .15;
            break;
    }
}

void Ghost::render(int time) {
    auto f = (float) (this->frame % 2);
    auto tx_w = 40.0;
    auto tx_h = 40.0;
    auto tx_x = f / 2.0;
    auto tx_y = 3.0 / 4;

    switch (this->direction) {
        case GhostDirection::Down:
            tx_y = 0.0 / 4;
            break;
        case GhostDirection::Up:
            tx_y = 1.0 / 4;
            break;
        case GhostDirection::Left:
            tx_y = 2.0 / 4;
            break;
        case GhostDirection::Right:
        default:
            tx_y = 3.0 / 4;
            break;
    }

    glBindTexture(GL_TEXTURE_2D, texture_id);

    glPushMatrix();
    glTranslatef(this->x, this->y, 0);

    glBegin(GL_POLYGON);
    glTexCoord2f(tx_x, tx_y);
    glVertex2f(0, 0);
    glTexCoord2f(tx_x + 1.0 / 2.0, tx_y);
    glVertex2f(tx_w, 0);
    glTexCoord2f(tx_x + 1.0 / 2.0, tx_y + 1.0 / 4.0);
    glVertex2f(tx_w, tx_h);
    glTexCoord2f(tx_x, tx_y + 1.0 / 4.0);
    glVertex2f(0, tx_h);
    glEnd();

    glPopMatrix();
}

void Ghost::setPosition(float x, float y) {
    this->x = x;
    this->y = y;
}

void Ghost::up(int time) {
    if (this->is_moving && this->direction == GhostDirection::Up) return;

    this->is_moving = true;
    this->start_move_time = time;
    this->direction = GhostDirection::Up;
}

void Ghost::down(int time) {
    if (this->is_moving && this->direction == GhostDirection::Down) return;

    this->is_moving = true;
    this->start_move_time = time;
    this->direction = GhostDirection::Down;
}

void Ghost::left(int time) {
    if (this->is_moving && this->direction == GhostDirection::Left) return;

    this->is_moving = true;
    this->start_move_time = time;
    this->direction = GhostDirection::Left;
}

void Ghost::right(int time) {
    if (this->is_moving && this->direction == GhostDirection::Right) {
        return;
    };

    this->is_moving = true;
    this->start_move_time = time;
    this->direction = GhostDirection::Right;
}

void Ghost::stop(int time) {
    this->is_moving = false;
}

float Ghost::getNextX() {
    float nextX;
    nextX = this->x;
    switch (this->direction) {
        case GhostDirection::Left:
            nextX -= .15;
            break;
        case GhostDirection::Right:
            nextX += .15;
            break;
        default:
            break;
    }

    return nextX;
}

float Ghost::getNextY() {
    float nextY;
    nextY = this->y;
    switch (this->direction) {
        case GhostDirection::Up:
            nextY += .15;
            break;
        case GhostDirection::Down:
            nextY -= .15;
            break;
        default:
            break;
    }
    return nextY;
}

void Ghost::changeDirection(int time) {
    switch (this->direction) {
        case GhostDirection::Up:
            left(time);
            break;
        case GhostDirection::Down:
            right(time);
            break;
        case GhostDirection::Left:
            down(time);
            break;
        case GhostDirection::Right:
            up(time);
            break;
        default:

            break;
    }
}

float Ghost::getX() {
    return this->x;
}

float Ghost::getY() {
    return this->y;
}